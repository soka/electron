Esta Web está dedicada a la programación de aplicaciones para escritorio con Electron basado en Node.js.

Web con la documentación en GitLab pages: https://soka.gitlab.io/electron


# Documentación MkDocs

La documentación esta escrita en MarkDown y la Web estática se genera con [MkDocs](https://www.mkdocs.org/), para generarla en la computadora local es necesario tener instalado Python y el modulo MkDocs.

Podemos generar el sitio con el siguiente comando:

```
> python -m mkdocs build
```

Podemos ejecutar un servidor básico local para visualizar la Web [http://127.0.0.1:8000/](http://127.0.0.1:8000/):

```
> python -m mkdocs serve
```

# Python

## Instalar PIP en Windows

PIP es una utilidad de línea de comandos para instalar paquetes [PyPI](https://pypi.org/).

Para instalar PIP descargamos el script [get-pip.py](https://bootstrap.pypa.io/get-pip.py) y lo ejecutamos con el siguiente comando

```
python get-pip.py
```

Para que la línea de comandos interprete PIP debemos añadirlo a las rutas

```
set PATH=%PATH%;C:\Users\i.landajuela\AppData\Local\Programs\Python\Python36-32\Scripts
```

**Enlaces**

* [https://www.makeuseof.com/tag/install-pip-for-python/](https://www.makeuseof.com/tag/install-pip-for-python/).


# Instalando nuevos themes para MkDocs

Ejemplos:

```
pip install mkdocs-material
pip install mkdocs-bootstrap 
```

Ejemplo del theme Boostrap:

![](img/01.png)


**Enlaces**:

* [https://github.com/mkdocs/mkdocs/wiki/MkDocs-Themes](https://github.com/mkdocs/mkdocs/wiki/MkDocs-Themes).




